import com.oo.*;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        // Press Alt+Enter with your caret at the highlighted text to see how
        // IntelliJ IDEA suggests fixing it.
        Car coolCar = new CoolCar(20, new GasolineEngine());
        Car bigTruck = new Truck(10, new GasolineEngine());
        Driver driver = new Driver(coolCar);
    }
}