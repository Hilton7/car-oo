package com.oo;

public abstract class Engine {
    protected int accelerate;

    public int getAccelerate() {
        return accelerate;
    }

    protected void setAccelerate(int accelerate) {
        this.accelerate = accelerate;
    }
}
