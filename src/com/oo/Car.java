package com.oo;

public abstract class Car {
    protected Engine engine;
    protected String name;
    protected int speed;
    protected int accelerated;


    public void display() {
        System.out.println(String.format("%s: speed up to %d km/h", name, speed));
    }

    public void speedUp() {
        this.speed += this.accelerated;
        display();
    }

    public int getAccelerated() {
        return accelerated;
    }

    public void setAccelerated(int accelerated) {
        this.accelerated = accelerated;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.accelerated = engine.getAccelerate();
    }
}
