package com.oo;

public class Driver {
    private Car car;

    public Driver(Car car) {
        this.car = car;
    }

    public Driver() {
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public void showMessage() {
        this.car.display();
    }

    public void speedUp() {
        this.car.speedUp();
    }
}
