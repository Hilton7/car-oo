package com.oo;

public class CoolCar extends Car {
    public CoolCar(int speed, Engine engine) {
        this.name = "Cool car";
        this.speed = speed;
        this.accelerated = engine.getAccelerate();
    }
}
