package com.oo;

public class Truck extends Car {
    public Truck(int speed, Engine engine) {
        this.name = "Big Truck";
        this.speed = speed;
        this.accelerated = engine.getAccelerate();
    }
}
